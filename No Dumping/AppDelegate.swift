//
//  AppDelegate.swift
//  No Dumping
//
//  Created by Mahil Arasu on 19/04/17.
//  Copyright © 2017 iqube. All rights reserved.
//

import UIKit

struct appURLs {
    let mobileAuthURL = "http://www." + "nodumping.in/mobile/auth/login"
    let liveRecoveryMeterURL = "http://www." + "nodumping.in/recovery/"
    let apartmentListURL = "http://www." + "nodumping.in/mobile/apartments"
    let addApartmentWasteURL = "http://www." + "nodumping.in/mobile/apartment/wastes/add"
    let getWardsByZone = "http://www." + "nodumping.in/mobile/wards/"
    let getPushcartsByWard = "http://www." + "nodumping.in/mobile/pushcarts/"
    let addWardWasteURL = "http://www." + "nodumping.in/mobile/ward/wastes/add"
    let addAwarnessMeetingURL = "http://www." + "nodumping.in/mobile/meeting/add"
    let addSBMMeetingURL = "http://www." + "nodumping.in/mobile/sbmmeeting/add"
}

let appURL = appURLs()

struct appConstants {
    var isLoggedIn: Bool!
    let isLoggedInKey = "isLoggedin"
    var isReachable: Bool!
    let isReachableKey = "isReachable"
    var initalVCIdentifier = ""
    var userID = 0
    var userIDKey = "userID"
    var userName = ""
    var userNameKey = "userName"
    var token = ""
    var tokenKey = "token"
    
}

var appConstant = appConstants()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var reachability = Reachability()

    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        self.loadConstants()
        return true
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //manage initial VC
        self.manageInitialVC()
        
        // reachability
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged),name: ReachabilityChangedNotification,object: reachability)
        do{
            try reachability?.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

extension AppDelegate {
    
    func reachabilityChanged(note: NSNotification) {
        
        let reachability = note.object as! Reachability
        
        if reachability.isReachable {
            if reachability.isReachableViaWiFi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
            appConstant.isReachable = true
            UserDefaults.standard.set(true, forKey: appConstant.isReachableKey)
        } else {
            print("Network not reachable")
            appConstant.isReachable = false
            UserDefaults.standard.set(false, forKey: appConstant.isReachableKey)
        }
    }
    
    func manageInitialVC() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController: UIViewController!
        appConstant.isLoggedIn = UserDefaults.standard.bool(forKey: appConstant.isLoggedInKey)
        print("log in status: \(appConstant.isLoggedIn!)")
        if appConstant.isLoggedIn! {
            initialViewController = storyBoard.instantiateViewController(withIdentifier: "mainNavigationController")
            appConstant.initalVCIdentifier = "mainNavigationController"
        } else {
            initialViewController = storyBoard.instantiateViewController(withIdentifier: "loginNavigationController")
            appConstant.initalVCIdentifier = "loginNavigationController"
        }
        self.window?.rootViewController = initialViewController
        self.window?.makeKeyAndVisible()
    }
    
    func changedRootVCToNavbar() -> Bool {
        
        if appConstant.initalVCIdentifier == "loginNavigationController" {
            // When LoginVC is the rootViewController change the rootViewController to NavbarVC
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyBoard.instantiateViewController(withIdentifier: "mainNavigationController")
            appConstant.initalVCIdentifier = "mainNavigationController"
            self.window?.rootViewController = controller
            self.window?.makeKeyAndVisible()
            return true
        } else {
            return false
        }
    }
    
    func loadConstants() {
        appConstant.isReachable = UserDefaults.standard.value(forKey: appConstant.isReachableKey) as! Bool!
        appConstant.isLoggedIn = UserDefaults.standard.value(forKey: appConstant.isLoggedInKey) as! Bool!
        if let userName = UserDefaults.standard.string(forKey: appConstant.userNameKey) {
            appConstant.userName = userName
        }
        
        appConstant.userID = UserDefaults.standard.integer(forKey: appConstant.userIDKey)
        
        if let token = UserDefaults.standard.string(forKey: appConstant.tokenKey) {
            appConstant.token = token
        }
    }
    
}

