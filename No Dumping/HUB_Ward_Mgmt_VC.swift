//
//  HUB_Ward_Mgmt_VC.swift
//  No Dumping
//
//  Created by Mahil Arasu on 24/05/17.
//  Copyright © 2017 iqube. All rights reserved.
//

import UIKit

class HUB_Ward_Mgmt_VC: UIViewController {

    @IBOutlet weak var loginNib: UIView!
    
    var loginViewObj = loginView()
    var isLoggedIn = false
    var isShowingLoginInView = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //Setup login View
        if !isShowingLoginInView {
            self.setupLoginView()
        }
    }
    
    func setupLoginView() {
        let frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.loginNib.frame.height)
        loginViewObj = loginView(frame: frame)
        loginNib.addSubview(loginViewObj)
        self.isShowingLoginInView = true
    }

}
