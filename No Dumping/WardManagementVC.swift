//
//  WardManagementVC.swift
//  No Dumping
//
//  Created by Mahil Arasu on 25/04/17.
//  Copyright © 2017 iqube. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation

struct addWardWaste {
    var userId = appConstant.userID
    var userToken = appConstant.token
    var userName = appConstant.userName
    var wardId = "", wardNo = "", organic = "", paper = "", plastic = ""
    var lat = 0.00, lon = 0.00
    var date = DateFormatter.localizedString(from: Date(), dateStyle: .short, timeStyle: .none)
    var image = ""
    var zone = "North", pushcartID = "", other = "", pushcartName = ""
    var total: String {
        get {
            if let organic = Int(self.organic), let paper = Int(self.paper), let plastic = Int(self.plastic), let other = Int(self.other) {
                let totalWaste = organic + paper + plastic + other
                return "\(totalWaste)"
            } else {
                return ""
            }
        }
    }
}
var wardWasteInfo = addWardWaste()
struct wardList {
    var id = 0, no = 0
}
var ward = [wardList]()
struct pushcartList {
    var id = "", name = ""
}
var pushcart = [pushcartList]()

class WardManagementVC: UIViewController {
    
    var shownPickerCell = -2
    
    var numberOfCellsInSec_0 = 1
    var numberOfCellsInSec_1 = 3
    var numberOfCellsInSec_2 = 4
    var numberOfCellsInSec_3 = 1
    var numberOfCellsInSec_4 = 1
    
    let pickerCellObj = pickerCellController()
    let textFiedCellObj = textFieldCellController()
    
    var cellUpdateIdentifier = ""

    @IBOutlet weak var informationCollectionView: UICollectionView!
    @IBOutlet weak var collectionLayout: UICollectionViewFlowLayout!
    
    let locationManager = CLLocationManager()
    
    var shouldRefreshData = true
    
    var isUploadButtonEnabled = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.informationCollectionView.delegate = self as UICollectionViewDelegate
        self.informationCollectionView.dataSource = self as UICollectionViewDataSource
        self.collectionLayout.minimumInteritemSpacing = 1.0
        self.collectionLayout.minimumLineSpacing = 1.0
        self.collectionLayout.sectionInset = UIEdgeInsetsMake(20, 0, 20, 0)
        self.collectionLayout.itemSize = CGSize(width: self.view.bounds.width - 8, height: 40.0)
        
        self.locationManager.requestWhenInUseAuthorization()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.loadWardsInzone), name:
            zoneChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.loadPushcartsInWard), name: wardsChangeNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //refresh the wardList obj and wardWasteInfo
        if self.shouldRefreshData {
            wardWasteInfo = addWardWaste()
            ward = []
            pushcart = []
            self.loadWardsInzone()
        }
        self.shouldRefreshData = true
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func loadWardsInzone() {
        //Load wards here
        if appConstant.isReachable! {
            //Do the online action
            let postParams: Parameters = ["user_id" : "\(appConstant.userID)", "_token" : "\(appConstant.token)"]
            Alamofire.request("\(appURL.getWardsByZone)\(wardWasteInfo.zone)", method: .post, parameters: postParams, encoding: URLEncoding.default, headers: nil).response(completionHandler: { (response) in
                if response.response?.statusCode == 200 {
                    self.parseWardList(JSONData: response.data!)
                    if ward.first?.id == nil {
                        let alertController = UIAlertController(title: "Session expired", message: "Please login again", preferredStyle: .alert)
                        let okButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
                        alertController.addAction(okButton)
                        self.present(alertController, animated: true, completion: nil)
                    } else {
                        //update cell
                        DispatchQueue.main.async {
                            self.cellUpdateIdentifier = "Ward"
                            var updateIndex = IndexPath(row: 1, section: 1)
                            if self.shownPickerCell == 0 {
                                updateIndex.row = 2
                            }
                            self.informationCollectionView.reloadItems(at: [updateIndex])
                        }
                        self.loadPushcartsInWard()
                    }
                }
            })
        } else {
            // Do offline action
            let alertController = UIAlertController(title: "You are offline", message: "Please check you internet connectivity", preferredStyle: .alert)
            let okButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okButton)
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    func loadPushcartsInWard() {
        if appConstant.isReachable! {
            //Do the online action
            let postParams: Parameters = ["user_id" : "\(appConstant.userID)", "_token" : "\(appConstant.token)"]
            Alamofire.request("\(appURL.getPushcartsByWard)\(wardWasteInfo.wardNo)", method: .post, parameters: postParams, encoding: URLEncoding.default, headers: nil).response(completionHandler: { (response) in
                if response.response?.statusCode == 200 {
                    self.parsePushcartList(JSONData: response.data!)
                    if pushcart.first?.id == nil {
                        let alertController = UIAlertController(title: "Session expired", message: "Please login again", preferredStyle: .alert)
                        let okButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
                        alertController.addAction(okButton)
                        self.present(alertController, animated: true, completion: nil)
                    } else {
                        //update cell
                        DispatchQueue.main.async {
                            self.cellUpdateIdentifier = "Pushcart"
                            var updateIndex = IndexPath(row: 2, section: 1)
                            if self.shownPickerCell != -2 {
                                updateIndex.row = 3
                            }
                            self.informationCollectionView.reloadItems(at: [updateIndex])
                        }
                    }
                }
            })
        } else {
            // Do offline action
            let alertController = UIAlertController(title: "You are offline", message: "Please check you internet connectivity", preferredStyle: .alert)
            let okButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okButton)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    //func to select picture
    func selectPicture() {
        //Select image via camera or photos app - do the selection in a action sheet controller
        let controller = UIAlertController(title: "Select from", message: "", preferredStyle: .actionSheet)
        let Camera = UIAlertAction(title: "Camera", style: .default) { (action) in
            //camera option
            let imageController = UIImagePickerController()
            imageController.sourceType = .camera
            imageController.delegate = self
            self.shouldRefreshData = false
            self.present(imageController, animated: true, completion: nil)
        }
        
        let Photos = UIAlertAction(title: "Photos", style: .default) { (action) in
            //photos option
            let imageController = UIImagePickerController()
            imageController.delegate = self
            imageController.sourceType = .photoLibrary
            self.shouldRefreshData = false
            self.present(imageController, animated: true, completion: nil)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        controller.addAction(Camera)
        controller.addAction(Photos)
        controller.addAction(cancel)
        self.present(controller, animated: true, completion: nil)
    }
    
    //func to convert image to Basse 64 encoding
    func performBase64EncodingOn(image: UIImage) {
        let encodingThread = DispatchQueue(label: "encodingThread")
        encodingThread.async {
            let imageData = UIImageJPEGRepresentation(image, 0.0)
            let base64encodedImage = imageData?.base64EncodedString(options: .endLineWithLineFeed)
            wardWasteInfo.image = base64encodedImage!
        }
    }
    
    //func to upload the wasate data to server
    func uploadAction() {
        
        if appConstant.isReachable! {
            //Do the online action
            if self.validateUpload() {
                
                self.isUploadButtonEnabled = false
                let updateIndex = IndexPath(row: 0, section: 4)
                self.informationCollectionView.reloadItems(at: [updateIndex])
                
                let postParameters: Parameters = [
                    "user_id": wardWasteInfo.userId,
                    "_token": wardWasteInfo.userToken,
                    "username": wardWasteInfo.userName,
                    "ward_id": wardWasteInfo.wardId,
                    "ward_no": wardWasteInfo.wardNo,
                    "organic": wardWasteInfo.organic,
                    "paper": wardWasteInfo.paper,
                    "plastic": wardWasteInfo.plastic,
                    "lat": wardWasteInfo.lat,
                    "longi": wardWasteInfo.lon,
                    "date": wardWasteInfo.date,
                    "image": wardWasteInfo.image,
                    "zone": wardWasteInfo.zone,
                    "pushcart_id": wardWasteInfo.pushcartID,
                    "other": wardWasteInfo.other,
                    "total": wardWasteInfo.total,
                    "pushcart_name": wardWasteInfo.pushcartName
                ]
                Alamofire.request(appURL.addWardWasteURL, method: .post, parameters: postParameters, encoding: URLEncoding.default, headers: nil).response(completionHandler: { (response) in
                    
                    let JSONString = JSON(data: response.data!)
                    
                    var alertController: UIAlertController!
                    if JSONString["message"].string! == "Waste added Successfully." {
                        
                        alertController = UIAlertController(title: "waste added successfully", message: "", preferredStyle: .alert)
                    } else {
                        alertController = UIAlertController(title: "Failed to upload", message: "Try again later", preferredStyle: .alert)
                    }
                    let okButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    alertController.addAction(okButton)
                    self.present(alertController, animated: true, completion: nil)
                    
                    
                    self.isUploadButtonEnabled = true
                    let updateIndex = IndexPath(row: 0, section: 4)
                    self.informationCollectionView.reloadItems(at: [updateIndex])
                    
                })
            } else {
                //Ask the user to fill all the fields
                var alertController: UIAlertController!
                if wardWasteInfo.lat == 0.00 || wardWasteInfo.lon == 0.00 {
                    alertController = UIAlertController(title: "Turn on location services", message: "", preferredStyle: .alert)
                } else {
                    alertController = UIAlertController(title: "Ensure all the fields are filled", message: "", preferredStyle: .alert)
                }
                let okButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alertController.addAction(okButton)
                self.present(alertController, animated: true, completion: nil)
            }
        } else {
            // Do offline action
            let alertController = UIAlertController(title: "You are offline", message: "Please check you internet connectivity", preferredStyle: .alert)
            let okButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okButton)
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    func validateUpload() -> Bool {
        if wardWasteInfo.lat != 0.00 && wardWasteInfo.lon != 0.00 && wardWasteInfo.organic != "" && wardWasteInfo.other != "" && wardWasteInfo.paper != "" && wardWasteInfo.plastic != "" && wardWasteInfo.pushcartID != "" && wardWasteInfo.pushcartName != "" && wardWasteInfo.total != "" && wardWasteInfo.userName != "" && wardWasteInfo.userToken != "" && wardWasteInfo.wardId != "" && wardWasteInfo.wardNo != "" && wardWasteInfo.zone != "" && wardWasteInfo.image != "" {
            return true
        } else {
            return false
        }
    }
    
}

extension WardManagementVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {        
        if section == 0 {
            return numberOfCellsInSec_0
        } else if section == 1 {
            return numberOfCellsInSec_1
        } else if section == 2 {
            return numberOfCellsInSec_2
        } else if section == 3 {
            return numberOfCellsInSec_3
        } else {
            return numberOfCellsInSec_4
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dateCell", for: indexPath) as! dateCellController
            let presentDate = DateFormatter.localizedString(from: Date(), dateStyle: .short, timeStyle: .none)
            cell.dateLabel.text = presentDate
            return cell
            
        } else if indexPath.section == 1 {
            if self.cellUpdateIdentifier != "" {
                //works when reloading cell at indexpath after loading all the wards in the given zone
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "menuCell", for: indexPath) as! menuCellControlller
                var titleText = ""
                var selectedtext = ""
                switch cellUpdateIdentifier {
                case "Ward":
                    titleText = "Ward No"
                    selectedtext = "\(wardWasteInfo.wardNo)"
                case "Pushcart":
                    titleText = "Pushcart Name"
                    selectedtext = "\(wardWasteInfo.pushcartName)"
                default:
                    titleText = " "
                    selectedtext = " "
                }
                
                cell.title.text = titleText
                cell.selectedLabel.text = selectedtext
                self.cellUpdateIdentifier = ""
                return cell
            } else if shownPickerCell + 1 != indexPath.row {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "menuCell", for: indexPath) as! menuCellControlller
                var titleText = ""
                var selectedtext = ""
                switch indexPath.row {
                case 0:
                    titleText = "Zone"
                    selectedtext = wardWasteInfo.zone
                case 1:
                    titleText = "Ward No"
                    selectedtext = "\(wardWasteInfo.wardNo)"
                case 2:
                    titleText = "Pushcart Name"
                    selectedtext = "\(wardWasteInfo.pushcartName)"
                default:
                    titleText = " "
                    selectedtext = " "
                }
                cell.title.text = titleText
                cell.selectedLabel.text = selectedtext
                return cell
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pickerCell", for: indexPath) as! pickerCellController
                switch indexPath.row {
                case 1:
                    self.pickerCellObj.pickerData = "Ward management - 5 zones"
                case 2:
                    self.pickerCellObj.pickerData = ward
                case 3:
                    self.pickerCellObj.pickerData = pushcart
                default:
                    self.pickerCellObj.pickerData = "Ward Zone - Error"
                }
                cell.picker.delegate = self.pickerCellObj
                cell.picker.dataSource = self.pickerCellObj
                return cell
             }
        } else if indexPath.section == 2 {
            //When section is 1, show the textFieldCell
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "textFieldCell", for: indexPath) as! textFieldCellController
            var titletext = ""
            switch indexPath.row {
            case 0:
                titletext = "Organic Waste"
            case 1:
                titletext = "Paper Waste"
            case 2:
                titletext = "Plastic Waste"
            case 3:
                titletext = "Other Waste"
            default:
                titletext = "Error"
            }
            cell.entry.delegate = self.textFiedCellObj
            cell.title.text = titletext
            cell.entry.placeholder = titletext
            return cell
        
         } else if indexPath.section == 3 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "buttonCell", for: indexPath) as! buttonCellController
            cell.buttonField.setTitle("Select Photo", for: .normal)
            cell.buttonField.addTarget(self, action: #selector(self.selectPicture), for: .touchUpInside)
            return cell
         } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "buttonCell", for: indexPath) as! buttonCellController
            cell.buttonField.setTitle("Upload", for: .normal)
            cell.buttonField.setTitle("Uploading...", for: .disabled)
            cell.buttonField.isEnabled = self.isUploadButtonEnabled
            cell.buttonField.addTarget(self, action: #selector(self.uploadAction), for: .touchUpInside)
            return cell
         }
        
    }
    
    //Display and hide the pickerCell
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.section == 1 {
            // When a cell is tapped and the picker isn't shown, then show the respective picker else, hide the respective picker
            
            if shownPickerCell == -2 {
                // Show the respective pickerCell
                if collectionView.cellForItem(at: indexPath)?.reuseIdentifier == "menuCell" {
                    shownPickerCell = indexPath.row
                    var pickerIndex = indexPath
                    pickerIndex.row = indexPath.row + 1
                    self.numberOfCellsInSec_1 = self.numberOfCellsInSec_1 + 1
                    collectionView.insertItems(at: [pickerIndex])
                }
            } else {
                //Delete the respective picker Cell
                if collectionView.cellForItem(at: indexPath)?.reuseIdentifier == "menuCell" {
                    var pickerIndex = indexPath
                    switch shownPickerCell {
                    case 0:
                        pickerIndex.row = 1
                    case 1:
                        pickerIndex.row = 2
                    case 2:
                        pickerIndex.row = 3
                    default:
                        pickerIndex.row = 0
                    }
                    shownPickerCell = -2
                    self.numberOfCellsInSec_1 = self.numberOfCellsInSec_1 - 1
                    collectionView.deleteItems(at: [pickerIndex])
                    //Need to update the selector cell
                    pickerIndex.row = pickerIndex.row - 1
                    collectionView.reloadItems(at: [pickerIndex])
                }
            }
        }
    }
    
    //Change the size of the pickerCell and menuCell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            //DateCell
            return CGSize(width: self.view.bounds.width - 8, height: 40.0)
        } else if indexPath.section == 1 {
            
            if shownPickerCell + 1 != indexPath.row {
                return CGSize(width: self.view.bounds.width - 8, height: 40.0)
            } else {
                var pickerIndex = 0
                switch shownPickerCell {
                case 0:
                    pickerIndex = 1
                case 1:
                    pickerIndex = 2
                case 2:
                    pickerIndex = 3
                default:
                    pickerIndex = 0
                }
                if pickerIndex == indexPath.row {
                    return CGSize(width: self.view.bounds.width - 8, height: 200.0)
                } else {
                    return CGSize(width: self.view.bounds.width - 8, height: 40.0)
                }
            }
        } else if indexPath.section == 2 {
            //When section is 2, Set the textFieldCell size
            return CGSize(width: self.view.bounds.width - 8, height: 80.0)
        } else {
            //for 3rd and 4th section, the size id is the same
            return CGSize(width: self.view.bounds.width - 8, height: 50.0)
        }
    }
    
}

//imagePickerControllerDelegate
extension WardManagementVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.performBase64EncodingOn(image: pickedImage)
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension WardManagementVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        wardWasteInfo.lat = locValue.latitude
        wardWasteInfo.lon = locValue.longitude
        locationManager.stopUpdatingLocation()
    }
}


//parse JSON data - using swiftyJSON
extension WardManagementVC {
    
    func parseWardList(JSONData: Data) {
        
        let json = JSON(data: JSONData)
        ward = []
        for (_, wardInfo) in json["results"] {
            var buffWardInfo = wardList()
            buffWardInfo.id = wardInfo["id"].int!
            buffWardInfo.no = wardInfo["ward_no"].int!
            ward.append(buffWardInfo)
        }
        
        if let id = ward.first?.id {
            wardWasteInfo.wardId = "\(id)"
        }
        if let no = ward.first?.no {
            wardWasteInfo.wardNo = "\(no)"
        }
        
    }
    
    func parsePushcartList(JSONData: Data) {
        let json = JSON(data: JSONData)
        pushcart = []
        for (_, pushcartInfo) in json["results"] {
            var buffpushcart = pushcartList()
            buffpushcart.id = "\(pushcartInfo["id"].int!)"
            buffpushcart.name = pushcartInfo["name"].string!
            pushcart.append(buffpushcart)
        }
        if let pushcartId = pushcart.first?.id {
            wardWasteInfo.pushcartID = pushcartId
        }
        if let pushcartName = pushcart.first?.name {
            wardWasteInfo.pushcartName = pushcartName
        }
    }
    
 }
