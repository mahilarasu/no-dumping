//
//  loginView.swift
//  No Dumping
//
//  Created by Mahil Arasu on 24/05/17.
//  Copyright © 2017 iqube. All rights reserved.
//

import UIKit

class loginView: UIView {

//    @IBOutlet weak var userName: UITextField!
//    @IBOutlet weak var password: UITextField!
    
    var view: UIView!
    
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        view = loadViewFromNib()
//        view.frame = bounds
        view.frame = CGRect(x: frame.minX, y: frame.minY, width: frame.width, height: frame.height)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "loginView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }

}
