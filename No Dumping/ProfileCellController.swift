//
//  ProfileCellController.swift
//  No Dumping
//
//  Created by Mahil Arasu on 19/04/17.
//  Copyright © 2017 iqube. All rights reserved.
//

import Foundation
import UIKit

class ProfileCell: UITableViewCell {
    
    
    @IBOutlet weak var UserImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    
    
}
