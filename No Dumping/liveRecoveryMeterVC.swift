//
//  liveRecoveryMeterVC.swift
//  No Dumping
//
//  Created by Mahil Arasu on 27/04/17.
//  Copyright © 2017 iqube. All rights reserved.
//

import UIKit

class liveRecoveryMeterVC: UIViewController {

    @IBOutlet weak var liveMeterWebView: UIWebView!
    @IBOutlet weak var webViewIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.liveMeterWebView.delegate = self
        
        self.liveMeterWebView.isHidden = true
        let url = URL(string: appURL.liveRecoveryMeterURL)
        let request = URLRequest(url: url!)
        self.liveMeterWebView.loadRequest(request)
        self.webViewIndicator.isHidden = false
        self.webViewIndicator.startAnimating()
    }

}

extension liveRecoveryMeterVC: UIWebViewDelegate {
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.webViewIndicator.isHidden = true
        self.webViewIndicator.stopAnimating()
        self.liveMeterWebView.isHidden = false
    }
    
}
