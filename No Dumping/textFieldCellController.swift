//
//  textFieldCellController.swift
//  No Dumping
//
//  Created by Mahil Arasu on 26/04/17.
//  Copyright © 2017 iqube. All rights reserved.
//

import UIKit

class textFieldCellController: UICollectionViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var entry: UITextField!
    
}

extension textFieldCellController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField.placeholder! {
        case "Number of flats":
            textField.keyboardType = .numberPad
        case "Number of members attending meeting":
            textField.keyboardType = .numberPad
        case "Ward number":
            textField.keyboardType = .numberPad
        case "Contact number":
            textField.keyboardType = .numberPad
        case "Number of participants":
            textField.keyboardType = .numberPad
        case "No of downloads":
            textField.keyboardType = .numberPad
        default:
            textField.keyboardType = .default
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.placeholder! {
        case "Dry waste":
            if let waste = textField.text {
                apartmentWasteInfo.dry = waste
            }
        case "Wet Waste":
            if let waste = textField.text {
                apartmentWasteInfo.wet = waste
            }
        case "Sanitary Waste":
            if let waste = textField.text {
                apartmentWasteInfo.hazardous = waste
            }
        case "Organic Waste":
            if let waste = textField.text {
                wardWasteInfo.organic = waste
            }
        case "Paper Waste":
            if let waste = textField.text {
                wardWasteInfo.paper = waste
            }
        case "Plastic Waste":
            if let waste = textField.text {
                wardWasteInfo.plastic = waste
            }
        case "Other Waste":
            if let waste = textField.text {
                wardWasteInfo.other = waste
            }
        case "Name of the location":
            if let meetingData = textField.text {
                awarnessMeetingInfo.nameOfLocation = meetingData
            }
        case "Number of flats":
            if let meetingData = textField.text {
                awarnessMeetingInfo.noOfFlats = meetingData
            }
        case "Address":
            if let meetingData = textField.text {
                awarnessMeetingInfo.address = meetingData
            }
        case "No dumping ambassador":
            if let meetingData = textField.text {
                awarnessMeetingInfo.noDumpingAmbassador = meetingData
            }
        case "No dumping apartment volunteer":
            if let meetingData = textField.text {
                awarnessMeetingInfo.noDumpingVolunteer = meetingData
            }
        case "Number of members attending meeting":
            if let meetingData = textField.text {
                awarnessMeetingInfo.noOfMembersInMeeting = meetingData
            }
        case "Note":
            if let meetingData = textField.text {
                awarnessMeetingInfo.note = meetingData
            }
        case "Purpose of meeting":
            if let SBMData = textField.text {
                SBMMeetingInfo.purposeOfMeeting = SBMData
            }
        case "Name of place":
            if let meetingData = textField.text {
                SBMMeetingInfo.nameOfPlace = meetingData
            }
        case "Ward number":
            if let meetingData = textField.text {
                SBMMeetingInfo.wardNo = meetingData
            }
        case "Resource persons":
            if let SBMData = textField.text {
                SBMMeetingInfo.resourcePerson = SBMData
            }
        case "Contact number":
            if let SBMData = textField.text {
                SBMMeetingInfo.contactNumber = SBMData
            }
        case "Number of participants":
            if let SBMData = textField.text {
                SBMMeetingInfo.NoOfParticipants = SBMData
            }
        case "Objective of meeting":
            if let SBMData = textField.text {
                SBMMeetingInfo.objectiveOfMeeting = SBMData
            }
        case "No of downloads":
            if let SBMData = textField.text {
                SBMMeetingInfo.noOfDownloads = SBMData
            }
        case "Feedback":
            if let SBMData = textField.text {
                SBMMeetingInfo.feedback = SBMData
            }
        default:
            print("Error finding textField")
        }

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //Decrease the scrollView height on dismissing the keyboard
        textField.resignFirstResponder()
        return true
    }
    
    
    
}
