//
//  loginVC.swift
//  No Dumping
//
//  Created by Mahil Arasu on 27/04/17.
//  Copyright © 2017 iqube. All rights reserved.
//

import UIKit
import Alamofire

class loginVC: UIViewController {
    
    struct authParams {
        var message = "", userName = "", phone = "", token = "", role = ""
        var isAuthenticated = false
        var userID: Int = 0
    }
    
    var authResult = authParams()

    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var loginStack: UIStackView!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var commentLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.phoneField.delegate = self
        self.passwordField.delegate = self
        self.commentLabel.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.manageUIElementsfor(loggedIn: appConstant.isLoggedIn!)
    }
    
    @IBAction func loginAction(_ sender: Any) {
            //login action
            self.loginAuth()
        self.loginButton.isEnabled = false
        self.loginButton.setTitle("Logging in...", for: .disabled)
    }
    
    @IBAction func logoutAction(_ sender: Any) {
        //logout action
        appConstant.isLoggedIn = false
        UserDefaults.standard.set(false, forKey: appConstant.isLoggedInKey)
        self.manageUIElementsfor(loggedIn: appConstant.isLoggedIn!)
    }
    
    func loginAuth() {
        
        if appConstant.isReachable! {
        
            let postString: Parameters = ["phone" : self.phoneField.text!, "password" : self.passwordField.text!, "gcm_id" : "asdadqweWQ"]
            
            Alamofire.request(appURL.mobileAuthURL, method: .post, parameters: postString, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                
                DispatchQueue.main.async {
                    self.loginButton.isEnabled = true
                    self.loginButton.setTitle("Log in", for: .normal)
                }
                
                self.parseAuth(JSONData: response.data!)
                if self.authResult.isAuthenticated {
                    //On correct credentials
                    appConstant.isLoggedIn = true
                    UserDefaults.standard.set(true, forKey: appConstant.isLoggedInKey)
                    appConstant.userName = self.authResult.userName
                    UserDefaults.standard.set(self.authResult.userName, forKey: appConstant.userNameKey)
                    appConstant.userID = self.authResult.userID
                    UserDefaults.standard.set(self.authResult.userID, forKey: appConstant.userIDKey)
                    appConstant.token = self.authResult.token
                    UserDefaults.standard.set(self.authResult.token, forKey: appConstant.tokenKey)
                    DispatchQueue.main.async {
                        self.presentDismissController(forBool: AppDelegate().changedRootVCToNavbar())
                    }
                } else {
                    //On wrong credentials.
                    self.commentLabel.isHidden = false
                    self.commentLabel.text = self.authResult.message
                    Timer.scheduledTimer(withTimeInterval: 4, repeats: false, block: { (time) in
                        self.commentLabel.isHidden = true
                    })
                }
            }
        } else {
            // Do offline action
            let alertController = UIAlertController(title: "You are offline", message: "Please check you internet connectivity", preferredStyle: .alert)
            let okButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okButton)
            self.present(alertController, animated: true, completion: nil)
            
            DispatchQueue.main.async {
                self.loginButton.isEnabled = true
                self.loginButton.setTitle("Log in", for: .normal)
            }
        }

    }
    
    func presentDismissController(forBool: Bool) {
        // if rc is mainNavigationController, dimiss.
        // if rc is loginNavigationController, present
        if appConstant.isLoggedIn! {
            if forBool {
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "mainNavigationController")
                self.present(controller!, animated: true, completion: nil)
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func manageUIElementsfor(loggedIn: Bool) {
        
        // when user logged in > true, don't hide logout button, hide loginStack, doneButton is enabled
        logoutButton.isHidden = !loggedIn
        loginStack.isHidden = loggedIn
        doneButton.isEnabled = loggedIn
        
    }
    
    @IBAction func doneAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension loginVC: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}

extension loginVC {
    
    func parseAuth(JSONData: Data) {
        
        //converting it to JSON
        let responseData = try! JSONSerialization.jsonObject(with: JSONData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: Any]
        
        if let userId = responseData["user_id"] as! Int! {
            authResult.userID = userId
        }
        if let userName = responseData["username"] as! String! {
            authResult.userName = userName
        }
        if let phone = responseData["phone"] as! String! {
            authResult.phone = phone
        }
        if let token = responseData["_token"] as! String! {
            authResult.token = token
        }
        if let role = responseData["role"] as! String! {
            authResult.role = role
        }
        if let authenticated = responseData["authenticated"] as! Bool! {
            authResult.isAuthenticated = authenticated
        }
        if let message = responseData["message"] as! String! {
            authResult.message = message
        }
    }
    
}

