//
//  ApartmentManagementVC.swift
//  No Dumping
//
//  Created by Mahil Arasu on 26/04/17.
//  Copyright © 2017 iqube. All rights reserved.
//


/*
 Sections in collectionView
 section - 0: dateCell - 1no
 section - 1: Select apartment, using menuCell & pickerCell - 1no
 section - 2: collect the weight of dry, wet and sanitary waste, using textFieldcell - 3no
 section - 3: 'Yes or No' for mixed waste, using menuCell & pickerCell - 1no
 section - 4: Photo select cell - buttonCell - 1no
 section - 5: Upload the date - buttonCell - 1no
*/

import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation

struct addApartmentWastes {
    var userId = appConstant.userID
    var userToken = appConstant.token, userName = appConstant.userName
    var apartmentId = 0
    var apartmentName = "Apartment Name"
    var wet = "0", dry = "0", hazardous = "0"
    var mixedWaste = ""
    var lat = 0.00, lon = 0.00
    var date = DateFormatter.localizedString(from: Date(), dateStyle: .short, timeStyle: .none)
    var image = ""
}

var apartmentWasteInfo = addApartmentWastes()

struct apartmentModel {
    var id = 0
    var name = ""
}
var apartmentModelObj = [apartmentModel]()

class ApartmentManagementVC: UIViewController {
    
    
    
    var isPickerCellShownInSec_1 = false
    var isPickerCellShownInSec_3 = false
    var numberOfCellsInSec_0 = 1
    var numberOfCellsInSec_1 = 1
    var numberOfCellsInSec_2 = 3
    var numberOfCellsInSec_3 = 1
    var numberOfCellsInSec_4 = 1
    var numberOfCellsInSec_5 = 1
    
    let pickerCellObj = pickerCellController()
    let textFiedCellObj = textFieldCellController()

    var isUploadButtonEnabled = true
    
    @IBOutlet weak var apartmentinfoView: UICollectionView!
    @IBOutlet weak var collectionLayout: UICollectionViewFlowLayout!
    
    var shouldRefreshData = true
    
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.apartmentinfoView.delegate = self
        self.apartmentinfoView.dataSource = self
        self.collectionLayout.minimumInteritemSpacing = 1.0
        self.collectionLayout.minimumLineSpacing = 1.0
        self.collectionLayout.sectionInset = UIEdgeInsetsMake(20, 0, 20, 0)
        self.collectionLayout.itemSize = CGSize(width: self.view.bounds.width - 8, height: 40.0)
        
        self.locationManager.requestWhenInUseAuthorization()
        
    }
    
    override func viewWillAppear(_ animated: Bool) { 
        
        //Refresh the model object and waste object
        if self.shouldRefreshData {
            apartmentWasteInfo = addApartmentWastes()
            apartmentModelObj = []
        }
        self.shouldRefreshData = true
        //Load the apartment list from server
        pullApartmentList { (isSuccess) in
            if isSuccess {
                let indexPath = IndexPath(row: 0, section: 1)
                self.apartmentinfoView.reloadItems(at: [indexPath])
            } else {
                let alertController = UIAlertController(title: "Session expired", message: "Please login again", preferredStyle: .alert)
                let okButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alertController.addAction(okButton)
                self.present(alertController, animated: true, completion: nil)
            }
        }
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func pullApartmentList(completion: @escaping ((Bool) -> Void)) {
        
        if appConstant.isReachable! {
            //Do the online action
            
            let postParams: Parameters = ["user_id" : "\(appConstant.userID)", "_token" : "\(appConstant.token)"]
            
            Alamofire.request(appURL.apartmentListURL, method: .post, parameters: postParams, encoding: URLEncoding.default, headers: nil).response(completionHandler: { (response) in
                if response.response?.statusCode == 200 {
                    self.parseApartmentList(JSONData: response.data!)
                }
                if apartmentModelObj.isEmpty {
                    return completion(false)
                } else {
                    return completion(true)
                }
            })
            
        } else {
            //Do the offline action
            // Do offline action
            let alertController = UIAlertController(title: "You are offline", message: "Please check you internet connectivity", preferredStyle: .alert)
            let okButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okButton)
            self.present(alertController, animated: true, completion: nil)
            completion(false)
        }
        
    }
    
    //func to select picture
    func selectPicture() {
        //Select image via camera or photos app - do the selection in a action sheet controller
        let controller = UIAlertController(title: "Select from", message: "", preferredStyle: .actionSheet)
        let Camera = UIAlertAction(title: "Camera", style: .default) { (action) in
            //camera option
            let imageController = UIImagePickerController()
            imageController.sourceType = .camera
            imageController.delegate = self
            self.shouldRefreshData = false
            self.present(imageController, animated: true, completion: nil)
        }
        
        let Photos = UIAlertAction(title: "Photos", style: .default) { (action) in
            //photos option
            let imageController = UIImagePickerController()
            imageController.delegate = self
            imageController.sourceType = .photoLibrary
            self.shouldRefreshData = false
            self.present(imageController, animated: true, completion: nil)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        controller.addAction(Camera)
        controller.addAction(Photos)
        controller.addAction(cancel)
        self.present(controller, animated: true, completion: nil)
    }
    
    //func to upload the wasate data to server
    func uploadAction() {
        
        if appConstant.isReachable! {
            //Do the online action
            if validateUpload() {
                
                //Disable upload button
                self.isUploadButtonEnabled = false
                let updateIndex = IndexPath(row: 0, section: 5)
                self.apartmentinfoView.reloadItems(at: [updateIndex])
                
                let postParams: Parameters = [
                    "user_id" : "\(apartmentWasteInfo.userId)",
                    "_token" : "\(apartmentWasteInfo.userToken)",
                    "username" : "\(apartmentWasteInfo.userName)",
                    "apartment_id" : "\(apartmentWasteInfo.apartmentId)",
                    "apartment_name" : "\(apartmentWasteInfo.apartmentName)",
                    "wet" : "\(apartmentWasteInfo.wet)",
                    "dry" : "\(apartmentWasteInfo.dry)",
                    "hazardous" : "\(apartmentWasteInfo.hazardous)",
                    "lat" : "\(apartmentWasteInfo.lat)",
                    "longi" : "\(apartmentWasteInfo.lon)",
                    "date" : "\(apartmentWasteInfo.date)",
                    "image" : "\(apartmentWasteInfo.image)",
                    "mixed_waste" : "\(apartmentWasteInfo.mixedWaste)"
                ]
                Alamofire.request(appURL.addApartmentWasteURL, method: .post, parameters: postParams, encoding: URLEncoding.default, headers: nil).response { (response) in
                    
                    let JSONString = JSON(data: response.data!)
                    
                    var alertController: UIAlertController!
                    if JSONString["message"].string! == "Waste added Successfully." {
                        alertController = UIAlertController(title: "waste added successfully", message: "", preferredStyle: .alert)
                    } else {
                        alertController = UIAlertController(title: "Failed to upload", message: "Try again later", preferredStyle: .alert)
                    }
                    let okButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    alertController.addAction(okButton)
                    self.present(alertController, animated: true, completion: nil)
                    
                    self.isUploadButtonEnabled = true
                    let updateIndex = IndexPath(row: 0, section: 5)
                    self.apartmentinfoView.reloadItems(at: [updateIndex])
                }
            } else {
                //Ask the user to fill all the fields
                var alertController: UIAlertController!
                if apartmentWasteInfo.lat == 0.00 || apartmentWasteInfo.lon == 0.00 {
                    alertController = UIAlertController(title: "Turn on location services", message: "", preferredStyle: .alert)
                } else {
                    alertController = UIAlertController(title: "Ensure all the fields are filled", message: "", preferredStyle: .alert)
                }
                let okButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alertController.addAction(okButton)
                self.present(alertController, animated: true, completion: nil)
            }
        } else {
            // Do offline action
            let alertController = UIAlertController(title: "You are offline", message: "Please check you internet connectivity", preferredStyle: .alert)
            let okButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okButton)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    //func to convert image to Basse 64 encoding
    func performBase64EncodingOn(image: UIImage) {
        let encodingThread = DispatchQueue(label: "encodingThread")
        encodingThread.async {
            let imageData = UIImageJPEGRepresentation(image, 0.0)
            let base64encodedImage = imageData?.base64EncodedString(options: .endLineWithLineFeed)
            apartmentWasteInfo.image = base64encodedImage!
        }
    }
    
    //func to validate the upload
    func validateUpload() -> Bool {
        
        if apartmentWasteInfo.apartmentId != 0 && apartmentWasteInfo.apartmentName != "Apartment Name" && apartmentWasteInfo.dry != "0" && apartmentWasteInfo.wet != "0" && apartmentWasteInfo.hazardous != "0" && apartmentWasteInfo.mixedWaste != "0" && apartmentWasteInfo.lat != 0.00 && apartmentWasteInfo.lon != 0.00 && apartmentWasteInfo.userId != -2 && apartmentWasteInfo.userToken != "" && apartmentWasteInfo.userName != "" {
            return true
        } else {
            return false
        }
        
    }

}


//Collection view delegate, datasource and the layout info.
extension ApartmentManagementVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return numberOfCellsInSec_0
        } else if section == 1 {
            return numberOfCellsInSec_1
        } else if section == 2 {
            return numberOfCellsInSec_2
        } else if section == 3 {
            return numberOfCellsInSec_3
        } else if section == 4 {
            return numberOfCellsInSec_4
        } else {
            return numberOfCellsInSec_5
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dateCell", for: indexPath) as! dateCellController
            let presentDate = DateFormatter.localizedString(from: Date(), dateStyle: .short, timeStyle: .none)
            cell.dateLabel.text = presentDate
            return cell
            
        } else if indexPath.section == 1 {
            
            if indexPath.row == 0  {
                //Display apartment name - pickerCell
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "menuCell", for: indexPath) as! menuCellControlller
                cell.title.text = apartmentWasteInfo.apartmentName
                cell.selectedLabel.text = ""
                return cell
             } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pickerCell", for: indexPath) as! pickerCellController
                self.pickerCellObj.pickerData = apartmentModelObj
                cell.picker.delegate = self.pickerCellObj
                cell.picker.dataSource = self.pickerCellObj
                return cell
            }
         } else if indexPath.section == 2 {
            //Display textFieldCell
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "textFieldCell", for: indexPath) as! textFieldCellController
            var titletext = ""
            switch indexPath.row {
            case 0:
                titletext = "Dry waste"
            case 1:
                titletext = "Wet Waste"
            case 2:
                titletext = "Sanitary Waste"
            default:
                titletext = "Error"
            }
            cell.entry.delegate = self.textFiedCellObj
            cell.title.text = titletext
            cell.title.restorationIdentifier = "Apartment \(titletext)"
            cell.entry.placeholder = titletext
            return cell
        } else if indexPath.section == 3 {
            //Get 'Yes or No' for mixed waste
            if indexPath.row == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "menuCell", for: indexPath) as! menuCellControlller
                cell.title.text = "Mixed waste "
                cell.selectedLabel.text = apartmentWasteInfo.mixedWaste
                return cell
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pickerCell", for: indexPath) as! pickerCellController
                self.pickerCellObj.pickerData = "Apartment Mixed waste - 'Yes or No'"
                cell.picker.delegate = self.pickerCellObj
                cell.picker.dataSource = self.pickerCellObj
                return cell
            }
         } else if indexPath.section == 4 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "buttonCell", for: indexPath) as! buttonCellController
            cell.buttonField.setTitle("Select Photo", for: .normal)
            cell.buttonField.addTarget(self, action: #selector(self.selectPicture), for: .touchUpInside)
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "buttonCell", for: indexPath) as! buttonCellController
            cell.buttonField.setTitle("Upload", for: .normal)
            cell.buttonField.setTitle("Uploading...", for: .disabled)
            cell.buttonField.isEnabled = self.isUploadButtonEnabled
            cell.buttonField.addTarget(self, action: #selector(self.uploadAction), for: .touchUpInside)
            return cell
        }
    }
    
    //Display and hide the pickerCell
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) { 
        
        if indexPath.section == 1 {
            // When a cell is tapped and the picker isn't shown, then show the respective picker else, hide the respective picker
            
            if isPickerCellShownInSec_1 == false {
                // Show the respective pickerCell
                if collectionView.cellForItem(at: indexPath)?.reuseIdentifier == "menuCell" {
                    isPickerCellShownInSec_1 = true
                    var pickerIndex = indexPath
                    pickerIndex.row = indexPath.row + 1
                    self.numberOfCellsInSec_1 = self.numberOfCellsInSec_1 + 1
                    collectionView.insertItems(at: [pickerIndex])
                }
            } else {
                //Delete the respective picker Cell
                if collectionView.cellForItem(at: indexPath)?.reuseIdentifier == "menuCell" {
                    var pickerIndex = indexPath
                    pickerIndex.row = 1
                    isPickerCellShownInSec_1 = false
                    self.numberOfCellsInSec_1 = self.numberOfCellsInSec_1 - 1
                    collectionView.deleteItems(at: [pickerIndex])
                    collectionView.reloadItems(at: [indexPath])
                }
            }
        } else if indexPath.section == 3 {
            //"yes or No' Mixed waste
            if isPickerCellShownInSec_3 == false {
                // Show the respective pickerCell
                if collectionView.cellForItem(at: indexPath)?.reuseIdentifier == "menuCell" {
                    isPickerCellShownInSec_3 = true
                    var pickerIndex = indexPath
                    pickerIndex.row = indexPath.row + 1
                    self.numberOfCellsInSec_3 = self.numberOfCellsInSec_3 + 1
                    collectionView.insertItems(at: [pickerIndex])
                }
            } else {
                //Delete the respective picker Cell
                if collectionView.cellForItem(at: indexPath)?.reuseIdentifier == "menuCell" {
                    var pickerIndex = indexPath
                    pickerIndex.row = 1
                    isPickerCellShownInSec_3 = false
                    self.numberOfCellsInSec_3 = self.numberOfCellsInSec_3 - 1
                    collectionView.deleteItems(at: [pickerIndex])
                    collectionView.reloadItems(at: [indexPath])
                }
            }
        }
    }
    
    //Change the size of the pickerCell and menuCell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize { 
        if indexPath.section == 0 {
            //DateCell
            return CGSize(width: self.view.bounds.width - 8, height: 40.0)
        } else if indexPath.section == 1 {
            
            if isPickerCellShownInSec_1 == false && numberOfCellsInSec_1 == 1 {
                return CGSize(width: self.view.bounds.width - 8, height: 40.0)
            } else {
                let pickerIndex = 1
                if pickerIndex == indexPath.row {
                    return CGSize(width: self.view.bounds.width - 8, height: 200.0)
                } else {
                    return CGSize(width: self.view.bounds.width - 8, height: 40.0)
                }
            }
        } else if indexPath.section == 2 {
            return CGSize(width: self.view.bounds.width - 8, height: 80.0)
        } else if indexPath.section == 3 {
            if isPickerCellShownInSec_1 == false && numberOfCellsInSec_3 == 1 {
                return CGSize(width: self.view.bounds.width - 8, height: 40.0)
            } else {
                let pickerIndex = 1
                if pickerIndex == indexPath.row {
                    return CGSize(width: self.view.bounds.width - 8, height: 200.0)
                } else {
                    return CGSize(width: self.view.bounds.width - 8, height: 40.0)
                }
            }
        } else {
            //for 4th and 5th section, the size id is the same
            return CGSize(width: self.view.bounds.width - 8, height: 50.0)
        }
    }

 }

//Parse JSON data - swiftyJSON
extension ApartmentManagementVC { 
    
    func parseApartmentList(JSONData: Data) {
        
        let json = JSON(data: JSONData)
        apartmentModelObj = []
        for (_, apartmentInfo) in json["results"] {
            var buffApartmentModelObj = apartmentModel()
            buffApartmentModelObj.name = apartmentInfo["name"].string!
            buffApartmentModelObj.id = apartmentInfo["id"].int!
            apartmentModelObj.append(buffApartmentModelObj)
        }
        if let id = apartmentModelObj.first?.id {
            apartmentWasteInfo.apartmentId = id
        }
        if let name = apartmentModelObj.first?.name {
            apartmentWasteInfo.apartmentName = name
        }
    }
    
}

//imagePickerControllerDelegate
extension ApartmentManagementVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.performBase64EncodingOn(image: pickedImage)
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension ApartmentManagementVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        apartmentWasteInfo.lat = locValue.latitude
        apartmentWasteInfo.lon = locValue.longitude
        locationManager.stopUpdatingLocation()
    }
}







