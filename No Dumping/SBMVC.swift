//
//  SBMVC.swift
//  No Dumping
//
//  Created by Mahil Arasu on 30/04/17.
//  Copyright © 2017 iqube. All rights reserved.
//

/*
 Sectoins
 1. section - 0: textFieldCell - 3
 2. section - 1: date          - 1
 3. section - 2: textFieldCell - 6
 4. section - 3: Voice         - 1
 5. section - 4: 3 photos      - 3
 6. section - 5: Upload        - 1
*/
import UIKit
import Alamofire

struct addSBMMeeting {
    var userId = appConstant.userID, userName = appConstant.userName, token = appConstant.token
    var purposeOfMeeting = "", nameOfPlace = "", wardNo = "", time = "", date = ""
    var resourcePerson = "", contactNumber = "", NoOfParticipants = ""
    var objectiveOfMeeting = "", noOfDownloads = "", feedback = ""
    var voiceRecord = ""
    var image1 = "", image2 = "", image3 = ""
}

var SBMMeetingInfo = addSBMMeeting()

class SBMVC: UIViewController {
    
    enum audioRecordingStages {
        case noRecordings
        case recordStarted
        case recordEnded
    }
    
    enum audioPlayingStages {
        case playing
        case notPlaying
    }
    
    lazy var recordStatus = audioRecordingStages.noRecordings
    lazy var playingStatus = audioPlayingStages.notPlaying

    var numberOfCellsInSec_0 = 3
    var numberOfCellsInSec_1 = 1
    var numberOfCellsInSec_2 = 6
    var numberOfCellsInSec_3 = 1
    var numberOfCellsInSec_4 = 3
    var numberOfCellsInSec_5 = 1
    
    var isUploadingData = false
    var isPickerCellShownInSec_1 = false
    var selectedPhotoCell = 0
    var recorder = Recorder()
    var textFieldCellObj = textFieldCellController()
    
    @IBOutlet weak var SBMInfoView: UICollectionView!
    @IBOutlet weak var collectionLayout: UICollectionViewFlowLayout!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.SBMInfoView.delegate = self
        self.SBMInfoView.dataSource = self
        self.collectionLayout.minimumInteritemSpacing = 1.0
        self.collectionLayout.minimumLineSpacing = 1.0
        self.collectionLayout.sectionInset = UIEdgeInsetsMake(20, 0, 20, 0)
        self.collectionLayout.itemSize = CGSize(width: self.view.bounds.width - 8, height: 40.0)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        SBMMeetingInfo = addSBMMeeting()
    }

    func getDateFrom(DatePicker: Any) {
        notificationCenter.post(name: SBMdateChangeNotification, object: nil)
    }
    
    func audioRecordAction() {
        
        if self.recordStatus == .noRecordings {
            self.recorder.Record("Record")
            self.recordStatus = .recordStarted
        } else if self.recordStatus == .recordStarted {
            self.recorder.Record("Record Stop")
            self.recordStatus = .recordEnded
            numberOfCellsInSec_3 = numberOfCellsInSec_3 + 1
            let playIndex = IndexPath(row: 1, section: 3)
            self.SBMInfoView.insertItems(at: [playIndex])
            let recordIndex = IndexPath(row: 0, section: 3)
            self.SBMInfoView.reloadItems(at: [recordIndex])
        }
        
        if self.recordStatus != .recordEnded {
            let recordIndex = IndexPath(row: 0, section: 3)
            self.SBMInfoView.reloadItems(at: [recordIndex])
        }
        
    }
    
    func audioPlayAction() {
        self.playingStatus = (self.playingStatus == .playing) ? .notPlaying : .playing
        if playingStatus == .playing {
            self.recorder.objectIdentifier = "SBM"
            notificationCenter.addObserver(self, selector: #selector(self.audioFinishPlaying), name: SBMplayFinishedNotification, object: nil)
            self.recorder.Play("Play")
        } else {
            self.recorder.Play("Stop")
        }
        
        let indexPath = IndexPath(row: 1, section: 3)
        self.SBMInfoView.reloadItems(at: [indexPath])
    }
    
    func audioFinishPlaying() {
        self.playingStatus = .notPlaying
        let indexPath = IndexPath(row: 1, section: 3)
        self.SBMInfoView.reloadItems(at: [indexPath])
    }
    
    func selectPicture1() {
        self.selectedPhotoCell = 1
        self.selectPicture()
    }
    
    func selectPicture2() {
        self.selectedPhotoCell = 2
        self.selectPicture()
    }
    
    func selectPicture3() {
        self.selectedPhotoCell = 3
        self.selectPicture()
    }
    
    //func to select picture
    func selectPicture() {
        //Select image via camera or photos app - do the selection in a action sheet controller
        let controller = UIAlertController(title: "Select from", message: "", preferredStyle: .actionSheet)
        let Camera = UIAlertAction(title: "Camera", style: .default) { (action) in
            //camera option
            let imageController = UIImagePickerController()
            imageController.sourceType = .camera
            imageController.delegate = self
            self.present(imageController, animated: true, completion: nil)
        }
        
        let Photos = UIAlertAction(title: "Photos", style: .default) { (action) in
            //photos option
            let imageController = UIImagePickerController()
            imageController.delegate = self
            imageController.sourceType = .photoLibrary
            self.present(imageController, animated: true, completion: nil)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        controller.addAction(Camera)
        controller.addAction(Photos)
        controller.addAction(cancel)
        self.present(controller, animated: true, completion: nil)
    }
    
    //func to convert image to Basse 64 encoding
    func performBase64EncodingOn(image: UIImage) {
        let encodingThread = DispatchQueue(label: "encodingThread")
        encodingThread.async {
            let imageData = UIImageJPEGRepresentation(image, 0.0)
            let base64encodedImage = imageData?.base64EncodedString(options: .endLineWithLineFeed)
            if self.selectedPhotoCell == 1 {
                SBMMeetingInfo.image1 = base64encodedImage!
            } else if self.selectedPhotoCell == 2 {
                SBMMeetingInfo.image2 = base64encodedImage!
            } else {
                SBMMeetingInfo.image3 = base64encodedImage!
            }
        }
    }
    
    func uploadData() {
        if appConstant.isReachable! {
            //Do the online action
            self.isUploadingData = true
            let indexpath = IndexPath(row: 0, section: 5)
            self.SBMInfoView.reloadItems(at: [indexpath])
            let postParams: Parameters = [
                "user_id" : "\(appConstant.userID)",
                "_token" : "\(appConstant.token)",
                "user_name" : "\(appConstant.userName)",
                "purpose_of_meeting" : "\(SBMMeetingInfo.purposeOfMeeting)",
                "name_of_place" : "\(SBMMeetingInfo.nameOfPlace)",
                "ward_no" : "\(SBMMeetingInfo.wardNo)",
                "time" : "\(SBMMeetingInfo.time)",
                "date" : "\(SBMMeetingInfo.date)",
                "resource_person" : "\(SBMMeetingInfo.resourcePerson)",
                "contact_number" : "\(SBMMeetingInfo.contactNumber)",
                "no_of_participants" : "\(SBMMeetingInfo.NoOfParticipants)",
                "objective_of_meeting" : "\(SBMMeetingInfo.objectiveOfMeeting)",
                "no_of_downloads" : "\(SBMMeetingInfo.noOfDownloads)",
                "feedback" : "\(SBMMeetingInfo.feedback)",
                "voice_record" : "\(SBMMeetingInfo.voiceRecord)",
                "image" : "\(SBMMeetingInfo.image1)",
                "image1" : "\(SBMMeetingInfo.image2)",
                "image2" : "\(SBMMeetingInfo.image3)"
            ]
            
            Alamofire.request(appURL.addSBMMeetingURL, method: .post, parameters: postParams, encoding: URLEncoding.default, headers: nil).response(completionHandler: { (response) in
                
                let jsonString = String(data: response.data!, encoding: .utf8)
                print(jsonString!)
                
                var alertController: UIAlertController!
                if (jsonString?.contains("{\"message\": \"SBM Meeting Successfully.\"}"))! {
                    
                    alertController = UIAlertController(title: "data added successfully", message: "", preferredStyle: .alert)
                }  else if (jsonString?.contains("Not authorized."))!{
                    alertController = UIAlertController(title: "Session expired", message: "Login again", preferredStyle: .alert)
                } else {
                    alertController = UIAlertController(title: "Failed to upload", message: "Try again later", preferredStyle: .alert)
                }
                let okButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alertController.addAction(okButton)
                self.present(alertController, animated: true, completion: nil)
                self.isUploadingData = false
                let indexpath = IndexPath(row: 0, section: 5)
                self.SBMInfoView.reloadItems(at: [indexpath])
            })
            
        } else {
            // Do offline action
            let alertController = UIAlertController(title: "You are offline", message: "Please check you internet connectivity", preferredStyle: .alert)
            let okButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okButton)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
}

extension SBMVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            // 3 textFieldCells
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "textFieldCell", for: indexPath) as! textFieldCellController
             var titleText = ""
            switch indexPath.row {
            case 0:
                titleText = "Purpose of meeting"
            case 1:
                titleText = "Name of place"
            case 2:
                titleText = "Ward number"
            default:
                titleText = ""
            }
            cell.entry.delegate = self.textFieldCellObj
            cell.title.text = titleText
            cell.entry.placeholder = titleText
            return cell
            
        } else if indexPath.section == 1 {
            //Get date and time
            if indexPath.row == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "menuCell", for: indexPath) as! menuCellControlller
                cell.title.text = "Date"
                cell.selectedLabel.text = "\(SBMMeetingInfo.date) \(SBMMeetingInfo.time)"
                
                return cell
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "datePickerCell", for: indexPath) as! datePickerCellController
                cell.picker.addTarget(self, action: #selector(self.getDateFrom(DatePicker:)), for: .valueChanged)
                cell.objectIdentifier = "SBM meeting"
                notificationCenter.addObserver(cell, selector: #selector(cell.updateDate), name: SBMdateChangeNotification, object: nil)
                return cell
            }
        } else if indexPath.section == 2 {
            //return 6 textFieldCells - collect 6 parameter info
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "textFieldCell", for: indexPath) as! textFieldCellController
            var titleText = ""
            switch indexPath.row {
            case 0:
                titleText = "Resource persons"
            case 1:
                titleText = "Contact number"
            case 2:
                titleText = "Number of participants"
            case 3:
                titleText = "Objective of meeting"
            case 4:
                titleText = "No of downloads"
            case 5:
                titleText = "Feedback"
            default:
                titleText = ""
            }
            cell.entry.delegate = self.textFieldCellObj
            cell.title.text = titleText
            cell.entry.placeholder = titleText
            return cell
        } else if indexPath.section == 3 {
            if indexPath.row == 0 {
                //Voice report - button cell
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "buttonCell", for: indexPath) as! buttonCellController
                cell.buttonField.addTarget(self, action: #selector(self.audioRecordAction), for: .touchUpInside)
                var buttonTitle = ""
                switch self.recordStatus {
                case .noRecordings:
                    buttonTitle = "Voice report - Record"
                case .recordStarted:
                    buttonTitle = "Voice report - Stop recording"
                case.recordEnded:
                    buttonTitle = "Voice report - Recorded"
                    cell.buttonField.setTitle(buttonTitle, for: .disabled)
                    cell.buttonField.isEnabled = false
                }
                cell.buttonField.setTitle(buttonTitle, for: .normal)
                
                return cell
                
            } else {
                //Voice report - button cell
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "buttonCell", for: indexPath) as! buttonCellController
                cell.buttonField.addTarget(self, action: #selector(self.audioPlayAction), for: .touchUpInside)
                if self.playingStatus == .notPlaying {
                    cell.buttonField.setTitle("Play", for: .normal)
                } else {
                    cell.buttonField.setTitle("Stop", for: .normal)
                }
                return cell
            }
        } else if indexPath.section == 4 {
            //Photo
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "buttonCell", for: indexPath) as! buttonCellController
            cell.buttonField.setTitle(" Select Photo", for: .normal)
            if indexPath.row == 0 {
                cell.buttonField.addTarget(self, action: #selector(self.selectPicture1), for: .touchUpInside)
            } else if indexPath.row == 1 {
                cell.buttonField.addTarget(self, action: #selector(self.selectPicture2), for: .touchUpInside)
            } else if indexPath.row == 2 {
                cell.buttonField.addTarget(self, action: #selector(self.selectPicture3), for: .touchUpInside)
            }
            return cell
        } else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "buttonCell", for: indexPath) as! buttonCellController
            if self.isUploadingData {
                cell.buttonField.isEnabled = false
                cell.buttonField.setTitle("Uploading...", for: .disabled)
            } else {
                cell.buttonField.isEnabled = true
                cell.buttonField.setTitle("Upload", for: .normal)
            }
            cell.buttonField.addTarget(self, action: #selector(self.uploadData), for: .touchUpInside)
            return cell
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            if !isPickerCellShownInSec_1 {
                self.isPickerCellShownInSec_1 = true
                self.numberOfCellsInSec_1 = self.numberOfCellsInSec_1 + 1
                var updateIndex = indexPath
                updateIndex.row = indexPath.row + 1
                self.SBMInfoView.insertItems(at: [updateIndex])
            } else {
                self.isPickerCellShownInSec_1 = false
                self.numberOfCellsInSec_1 = self.numberOfCellsInSec_1 - 1
                var updateIndex = indexPath
                updateIndex.row = indexPath.row + 1
                self.SBMInfoView.deleteItems(at: [updateIndex])
                self.SBMInfoView.reloadItems(at: [indexPath])
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.section == 0 || indexPath.section == 2 {
            return CGSize(width: self.view.bounds.width - 8, height: 80.0)
        } else if indexPath.section == 1 {
            if indexPath.row == 0 {
                return CGSize(width: self.view.bounds.width - 8, height: 40.0)
            } else {
                return CGSize(width: self.view.bounds.width - 8, height: 200.0)
            }
        } else {
            return CGSize(width: self.view.bounds.width - 8, height: 50.0)
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return numberOfCellsInSec_0
        case 1:
            return numberOfCellsInSec_1
        case 2:
            return numberOfCellsInSec_2
        case 3:
            return numberOfCellsInSec_3
        case 4:
            return numberOfCellsInSec_4
        case 5:
            return numberOfCellsInSec_5
        default:
            return 0
        }
    }
    
}

extension SBMVC:  UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.performBase64EncodingOn(image: pickedImage)
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}
