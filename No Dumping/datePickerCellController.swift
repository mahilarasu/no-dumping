//
//  datePickerCellController.swift
//  No Dumping
//
//  Created by Mahil Arasu on 30/04/17.
//  Copyright © 2017 iqube. All rights reserved.
//

import UIKit


public let awanrnessMeetingdateChangeNotification = NSNotification.Name(rawValue: "awanrnessMeetingdateChangeNotification")
public let SBMdateChangeNotification = NSNotification.Name(rawValue: "SBMdateChangeNotification")
class datePickerCellController: UICollectionViewCell {
    
    @IBOutlet weak var picker: UIDatePicker!
    var objectIdentifier = ""
    
    func updateDate() {
        let myDate = self.picker.date
        let dateFormatter = DateFormatter()
         dateFormatter.dateFormat = "dd-MM-yyyy"
        let tempDate = dateFormatter.string(from: myDate)
        
        dateFormatter.dateFormat = "HH:mm"
        let tempTime = dateFormatter.string(from: myDate)
        
        
        if objectIdentifier == "Awarness meeting" {
            awarnessMeetingInfo.date = tempDate
            awarnessMeetingInfo.time = tempTime
        } else if objectIdentifier == "SBM meeting" {
            SBMMeetingInfo.date = tempDate
            SBMMeetingInfo.time = tempTime
        }
    }
    
}
