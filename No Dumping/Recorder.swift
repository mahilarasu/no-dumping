//
//  Recorder.swift
//  No Dumping
//
//  Created by Mahil Arasu on 22/05/17.
//  Copyright © 2017 iqube. All rights reserved.
//

import Foundation
import AVFoundation

public let awarnessMeetingplayFinishedNotification = NSNotification.Name(rawValue: "awarnessMeetingplayFinishedNotification")
public let SBMplayFinishedNotification = NSNotification.Name(rawValue: "SBMplayFinishedNotification")

class Recorder: NSObject, AVAudioPlayerDelegate, AVAudioRecorderDelegate {
    
    var soundRecorder : AVAudioRecorder!
    var SoundPlayer : AVAudioPlayer!
    
    var AudioFileName = "sound.mp3"
    
    var objectIdentifier = ""
    
    let recordSettings = [AVSampleRateKey : NSNumber(value: Float(44100.0) as Float),
                          AVFormatIDKey : NSNumber(value: Int32(kAudioFormatMPEG4AAC) as Int32),
                          AVNumberOfChannelsKey : NSNumber(value: 1 as Int32),
                          AVEncoderAudioQualityKey : NSNumber(value: Int32(AVAudioQuality.medium.rawValue) as Int32)]
    
    override init() {
        super.init()
        self.setupRecorder()
    }
    
    func Record(_ sender: String) {
        if sender == "Record" {
            soundRecorder.record()
        }
        else{
            soundRecorder.stop()
        }
        
    }
    
    func Play(_ sender: String) {
        if sender == "Play" {
            preparePlayer()
            SoundPlayer.play()
        }
        else{
            SoundPlayer.stop()
        }
    }
    
    
    func preparePlayer(){
        
        do {
            try SoundPlayer = AVAudioPlayer(contentsOf: directoryURL()!)
            SoundPlayer.delegate = self
            SoundPlayer.prepareToPlay()
            SoundPlayer.volume = 1.0
        } catch {
            print("Error playing")
        }
        
    }
    
    func setupRecorder(){

        let audioSession:AVAudioSession = AVAudioSession.sharedInstance()
        
        //ask for permission
        if (audioSession.responds(to: #selector(AVAudioSession.requestRecordPermission(_:)))) {
            AVAudioSession.sharedInstance().requestRecordPermission({(granted: Bool)-> Void in
                if granted {
                    print("granted permission to record")
                    
                    //set category and activate recorder session
                    do {
                        
                        try audioSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
                        try self.soundRecorder = AVAudioRecorder(url: self.directoryURL()!, settings: self.recordSettings)
                        self.soundRecorder.prepareToRecord()
                        
                    } catch {
                        
                        print("Error Recording");
                        
                    }
                    
                }
            })
        }
        
    }
    
    func directoryURL() -> URL? {
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = urls[0] as URL
        let soundURL = documentDirectory.appendingPathComponent("sound.m4a")
        return soundURL
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if self.objectIdentifier == "awarness meeting" {
            notificationCenter.post(name: awarnessMeetingplayFinishedNotification, object: nil)
        } else if self.objectIdentifier == "SBM" {
            notificationCenter.post(name: SBMplayFinishedNotification, object: nil)
        }
    }
    
}
