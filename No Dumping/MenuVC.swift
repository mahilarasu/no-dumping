//
//  MenuVC.swift
//  No Dumping
//
//  Created by Mahil Arasu on 19/04/17.
//  Copyright © 2017 iqube. All rights reserved.
//

import Foundation
import UIKit

class MenuVC: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 7
        } else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Show the modules
        let cell = tableView.dequeueReusableCell(withIdentifier: "Module", for: indexPath) as! ModuleCell
        var name = ""
//        var image = UIImage()
        switch indexPath.row {
        case 0:
            name = "Dashboard"
        case 1:
            name = "HUB"
        case 2:
            name = "Live Recovery Meter"
        case 3:
            name = "Awarness Meeting"
        case 4:
            name = "SBM"
        case 5:
            name = "Status"
        case 6:
            name = "About Us"
        default:
            name = "Error"
        }
        cell.name.text = name
        cell.imageView?.image = UIImage()
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.backgroundColor = UIColor.darkGray
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            var segueID = ""
            switch indexPath.row {
            case 0:
                segueID = "Dashboard Segue"
            case 1:
                segueID = "HUB Segue"
            case 2:
                segueID = "LiveRecoveryMeter Segue"
            case 3:
                segueID = "Awarness Meeting Segue"
            case 4:
                segueID = "SBM Segue"
            case 5:
                segueID = "Status Segue"
            case 6:
                segueID = "About us Segue"
            default:
                segueID = ""
            }
            if segueID != "" {
                performSegue(withIdentifier: segueID, sender: nil)
            }
        }
    }
    
    @IBAction func profileAction(_ sender: Any) {
        let controller = storyboard?.instantiateViewController(withIdentifier: "loginNavigationController")
        self.present(controller!, animated: true, completion: nil)
    }
    
}










