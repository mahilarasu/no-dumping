//
//  ModuleCellController.swift
//  No Dumping
//
//  Created by Mahil Arasu on 19/04/17.
//  Copyright © 2017 iqube. All rights reserved.
//

import Foundation
import UIKit

class ModuleCell: UITableViewCell {
    
    @IBOutlet weak var moduleImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    
}
