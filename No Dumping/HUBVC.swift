//
//  HUBVC.swift
//  No Dumping
//
//  Created by Mahil Arasu on 24/05/17.
//  Copyright © 2017 iqube. All rights reserved.
//

import UIKit

class HUBVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func performWardSegue(_ sender: Any) {
        performSegue(withIdentifier: "HUB-Ward-mgmt-segue", sender: nil)
    }

    @IBAction func performApartmentSegue(_ sender: Any) {
        performSegue(withIdentifier: "HUB-Apartment-mgmt-segue", sender: nil)
    }
    

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }

}
