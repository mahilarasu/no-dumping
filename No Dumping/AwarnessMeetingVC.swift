//
//  AwarnessMeetingVC.swift
//  No Dumping
//
//  Created by Mahil Arasu on 30/04/17.
//  Copyright © 2017 iqube. All rights reserved.
//

/*
 Sections in collection view
 1. name of the location - textFieldCell                - 1
 2. date & time  - pickerCell                           - 1
 3. textFieldCell                                       - 6
 4. voice report - buttonCell                           - 1
 5. 3 photos - buttonCell                               - 3
 6. upload - buttonCell                                 - 1
*/

//https://passwords.google.com/settings/passwords

import UIKit
import Alamofire

struct addAwarnessMeeting {
    var userId = appConstant.userID, userName = appConstant.userName, token = appConstant.token
    var nameOfLocation = ""
    var date = ""
    var time = ""
    var noOfFlats = ""
    var address = ""
    var noDumpingAmbassador = ""
    var noDumpingVolunteer = ""
    var noOfMembersInMeeting = ""
    var currentLocatin = ""
    var note = ""
    var voiceRecord = ""
    var image1 = "", image2 = "", image3 = ""
}

var awarnessMeetingInfo = addAwarnessMeeting()

class AwarnessMeetingVC: UIViewController {
    
    enum audioRecordingStages {
        case noRecordings
        case recordStarted
        case recordEnded
    }
    
    enum audioPlayingStages {
        case playing
        case notPlaying
    }
    
    lazy var recordStatus = audioRecordingStages.noRecordings
    lazy var playingStatus = audioPlayingStages.notPlaying
    
    var numberOfCellsInSec_0 = 1
    var numberOfCellsInSec_1 = 1
    var numberOfCellsInSec_2 = 6
    var numberOfCellsInSec_3 = 1
    var numberOfCellsInSec_4 = 3
    var numberOfCellsInSec_5 = 1

    var isPickerCellShownInSec_1 = false
    var pickerCellObj = pickerCellController()
    var recorder = Recorder()
    let textFiedCellObj = textFieldCellController()
    var selectedPhotoCell = 0
    var isUploadingData = false
    
    
    @IBOutlet weak var meetingInfo: UICollectionView!
    @IBOutlet weak var collectionLayout: UICollectionViewFlowLayout!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.meetingInfo.delegate = self
        self.meetingInfo.dataSource = self
        self.collectionLayout.minimumInteritemSpacing = 1.0
        self.collectionLayout.minimumLineSpacing = 1.0
        self.collectionLayout.sectionInset = UIEdgeInsetsMake(20, 0, 20, 0)
        self.collectionLayout.itemSize = CGSize(width: self.view.bounds.width - 8, height: 40.0)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        awarnessMeetingInfo = addAwarnessMeeting()
    }
    func getDateFrom(DatePicker: Any) {
        notificationCenter.post(name: awanrnessMeetingdateChangeNotification, object: nil)
    }
    
    func audioRecordAction() {
        
        if self.recordStatus == .noRecordings {
            self.recorder.Record("Record")
            self.recordStatus = .recordStarted
        } else if self.recordStatus == .recordStarted {
            self.recorder.Record("Record Stop")
            self.recordStatus = .recordEnded
            numberOfCellsInSec_3 = numberOfCellsInSec_3 + 1
            let playIndex = IndexPath(row: 1, section: 3)
            self.meetingInfo.insertItems(at: [playIndex])
            let recordIndex = IndexPath(row: 0, section: 3)
            self.meetingInfo.reloadItems(at: [recordIndex])
            
            //let recordDirectry = self.recorder.directoryURL()!
            //do {
                //let audioFile = try NSString(contentsOf: recordDirectry, encoding: UInt.allZeros)
            //} catch {
             //   print("Unable to retrieve the file")
            //}
        }
        
        if self.recordStatus != .recordEnded {
            let recordIndex = IndexPath(row: 0, section: 3)
            self.meetingInfo.reloadItems(at: [recordIndex])
        }
        
    }
    
    func audioPlayAction() {
        self.playingStatus = (self.playingStatus == .playing) ? .notPlaying : .playing
        if playingStatus == .playing {
            
            self.recorder.objectIdentifier = "awarness meeting"
            notificationCenter.addObserver(self, selector: #selector(self.audioFinishPlaying), name: awarnessMeetingplayFinishedNotification, object: nil)
            
            self.recorder.Play("Play")
        } else {
            self.recorder.Play("Stop")
        }
        
        let indexPath = IndexPath(row: 1, section: 3)
        self.meetingInfo.reloadItems(at: [indexPath])
    }
    
    func audioFinishPlaying() {
        self.playingStatus = .notPlaying
        let indexPath = IndexPath(row: 1, section: 3)
        self.meetingInfo.reloadItems(at: [indexPath])
    }
    
    func selectPicture1() {
        self.selectedPhotoCell = 1
        self.selectPicture()
    }
    
    func selectPicture2() {
        self.selectedPhotoCell = 2
        self.selectPicture()
    }
    
    func selectPicture3() {
        self.selectedPhotoCell = 3
        self.selectPicture()
    }
    
    //func to select picture
    func selectPicture() {
        //Select image via camera or photos app - do the selection in a action sheet controller
        let controller = UIAlertController(title: "Select from", message: "", preferredStyle: .actionSheet)
        let Camera = UIAlertAction(title: "Camera", style: .default) { (action) in
            //camera option
            let imageController = UIImagePickerController()
            imageController.sourceType = .camera
            imageController.delegate = self
            self.present(imageController, animated: true, completion: nil)
        }
        
        let Photos = UIAlertAction(title: "Photos", style: .default) { (action) in
            //photos option
            let imageController = UIImagePickerController()
            imageController.delegate = self
            imageController.sourceType = .photoLibrary
            self.present(imageController, animated: true, completion: nil)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        controller.addAction(Camera)
        controller.addAction(Photos)
        controller.addAction(cancel)
        self.present(controller, animated: true, completion: nil)
    }
    
    //func to convert image to Basse 64 encoding
    func performBase64EncodingOn(image: UIImage) {
        let encodingThread = DispatchQueue(label: "encodingThread")
        encodingThread.async {
            let imageData = UIImageJPEGRepresentation(image, 0.0)
            let base64encodedImage = imageData?.base64EncodedString(options: .endLineWithLineFeed)
            if self.selectedPhotoCell == 1 {
                awarnessMeetingInfo.image1 = base64encodedImage!
            } else if self.selectedPhotoCell == 2 {
                awarnessMeetingInfo.image2 = base64encodedImage!
            } else {
                awarnessMeetingInfo.image3 = base64encodedImage!
            }
        }
    }
    
    func uploadData() {
        if appConstant.isReachable! {
            //Do the online action
            self.isUploadingData = true
            let indexpath = IndexPath(row: 0, section: 5)
            self.meetingInfo.reloadItems(at: [indexpath])
            let postParams: Parameters = [
                "user_id" : "\(appConstant.userID)",
                "_token" : "\(appConstant.token)",
                "user_name" : "\(appConstant.userName)",
                "name_of_location" : "\(awarnessMeetingInfo.nameOfLocation)",
                "date" : "\(awarnessMeetingInfo.date)",
                "number_of_flats" : "\(awarnessMeetingInfo.noOfFlats)",
                "address" : "\(awarnessMeetingInfo.address)",
                "time" : "\(awarnessMeetingInfo.time)",
                "no_dumping_ambassador" : "\(awarnessMeetingInfo.noDumpingAmbassador)",
                "no_dump_appartment_volunteer" : "\(awarnessMeetingInfo.noDumpingVolunteer)",
                "number_of_members_attended_in_meeting" : "\(awarnessMeetingInfo.noOfMembersInMeeting)",
                "location_share" : "\(awarnessMeetingInfo.nameOfLocation)",
                "note" : "\(awarnessMeetingInfo.note)",
                "voice_record" : "\(awarnessMeetingInfo.voiceRecord)",
                "image" : "\(awarnessMeetingInfo.image1)",
                "image1" : "\(awarnessMeetingInfo.image2)",
                "image2" : "\(awarnessMeetingInfo.image3)"
            ]
            
            Alamofire.request(appURL.addAwarnessMeetingURL, method: .post, parameters: postParams, encoding: URLEncoding.default, headers: nil).response(completionHandler: { (response) in
                
                let jsonString = String(data: response.data!, encoding: .utf8)
                print(jsonString!)
                
                var alertController: UIAlertController!
                if (jsonString?.contains("{\"message\": \"Awarness Meeting Successfully.\"}"))! {

                    alertController = UIAlertController(title: "data added successfully", message: "", preferredStyle: .alert)
                }  else if (jsonString?.contains("Not authorized."))!{
                    alertController = UIAlertController(title: "Session expired", message: "Login again", preferredStyle: .alert)
                } else {
                    alertController = UIAlertController(title: "Failed to upload", message: "Try again later", preferredStyle: .alert)
                }
                let okButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alertController.addAction(okButton)
                self.present(alertController, animated: true, completion: nil)
                self.isUploadingData = false
                let indexpath = IndexPath(row: 0, section: 5)
                self.meetingInfo.reloadItems(at: [indexpath])
            })

        } else {
            // Do offline action
            let alertController = UIAlertController(title: "You are offline", message: "Please check you internet connectivity", preferredStyle: .alert)
            let okButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okButton)
            self.present(alertController, animated: true, completion: nil)
        }
    }

    
}

extension AwarnessMeetingVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            // Name of the location - textFieldCell
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "textFieldCell", for: indexPath) as! textFieldCellController
            cell.entry.delegate = self.textFiedCellObj
            cell.title.text = "Name of the location"
            cell.entry.placeholder = "Name of the location"
            return cell
        } else if indexPath.section == 1 {
            //Get date and time
            if indexPath.row == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "menuCell", for: indexPath) as! menuCellControlller
                cell.title.text = "Date"
                cell.selectedLabel.text = "\(awarnessMeetingInfo.date) \(awarnessMeetingInfo.time)"
                return cell
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "datePickerCell", for: indexPath) as! datePickerCellController
                cell.picker.addTarget(self, action: #selector(self.getDateFrom(DatePicker:)), for: .valueChanged)
                cell.objectIdentifier = "Awarness meeting"
                notificationCenter.addObserver(cell, selector: #selector(cell.updateDate), name: awanrnessMeetingdateChangeNotification, object: nil)
                return cell
            }
        } else if indexPath.section == 2 {
            //return 6 textFieldCells - collect 6 parameter info
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "textFieldCell", for: indexPath) as! textFieldCellController
            var titleText = ""
            switch indexPath.row {
            case 0:
                titleText = "Number of flats"
            case 1:
                titleText = "Address"
            case 2:
                titleText = "No dumping ambassador"
            case 3:
                titleText = "No dumping apartment volunteer"
            case 4:
                titleText = "Number of members attending meeting"
            case 5:
                titleText = "Note"
            default:
                titleText = ""
            }
            cell.entry.delegate = self.textFiedCellObj
            cell.title.text = titleText
            cell.entry.placeholder = titleText
            return cell
        } else if indexPath.section == 3 {
            if indexPath.row == 0 {
                //Voice report - button cell
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "buttonCell", for: indexPath) as! buttonCellController
                cell.buttonField.addTarget(self, action: #selector(self.audioRecordAction), for: .touchUpInside)
                var buttonTitle = ""
                switch self.recordStatus {
                case .noRecordings:
                    buttonTitle = "Voice report - Record"
                case .recordStarted:
                    buttonTitle = "Voice report - Stop recording"
                case.recordEnded:
                    buttonTitle = "Voice report - Recorded"
                    cell.buttonField.setTitle(buttonTitle, for: .disabled)
                    cell.buttonField.isEnabled = false
                }
                cell.buttonField.setTitle(buttonTitle, for: .normal)
                
                return cell
                
            } else {
                //Voice report - button cell
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "buttonCell", for: indexPath) as! buttonCellController
                cell.buttonField.addTarget(self, action: #selector(self.audioPlayAction), for: .touchUpInside)
                if self.playingStatus == .notPlaying {
                    cell.buttonField.setTitle("Play", for: .normal)
                } else {
                    cell.buttonField.setTitle("Stop", for: .normal)
                }
                
                return cell
            }
        } else if indexPath.section == 4 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "buttonCell", for: indexPath) as! buttonCellController
            cell.buttonField.setTitle(" Select Photo", for: .normal)
            if indexPath.row == 0 {
                cell.buttonField.addTarget(self, action: #selector(self.selectPicture1), for: .touchUpInside)
            } else if indexPath.row == 1 {
                cell.buttonField.addTarget(self, action: #selector(self.selectPicture2), for: .touchUpInside)
            } else if indexPath.row == 2 {
                cell.buttonField.addTarget(self, action: #selector(self.selectPicture3), for: .touchUpInside)
            }
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "buttonCell", for: indexPath) as! buttonCellController
            if self.isUploadingData {
                cell.buttonField.isEnabled = false
                cell.buttonField.setTitle("Uploading...", for: .disabled)
            } else {
                cell.buttonField.isEnabled = true
                cell.buttonField.setTitle("Upload", for: .normal)
            }
            cell.buttonField.addTarget(self, action: #selector(self.uploadData), for: .touchUpInside)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            if !isPickerCellShownInSec_1 {
                self.isPickerCellShownInSec_1 = true
                self.numberOfCellsInSec_1 = self.numberOfCellsInSec_1 + 1
                var updateIndex = indexPath
                updateIndex.row = indexPath.row + 1
                self.meetingInfo.insertItems(at: [updateIndex])
            } else {
                self.isPickerCellShownInSec_1 = false
                self.numberOfCellsInSec_1 = self.numberOfCellsInSec_1 - 1
                var updateIndex = indexPath
                updateIndex.row = indexPath.row + 1
                self.meetingInfo.deleteItems(at: [updateIndex])
                self.meetingInfo.reloadItems(at: [indexPath])
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.section == 0 || indexPath.section == 2 {
            return CGSize(width: self.view.bounds.width - 8, height: 80.0)
        } else if indexPath.section == 1 {
            if indexPath.row == 0 {
                return CGSize(width: self.view.bounds.width - 8, height: 40.0)
            } else {
                return CGSize(width: self.view.bounds.width - 8, height: 200.0)
            }
        } else {
            return CGSize(width: self.view.bounds.width - 8, height: 50.0)
        }
        
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return numberOfCellsInSec_0
        case 1:
            return numberOfCellsInSec_1
        case 2:
            return numberOfCellsInSec_2
        case 3:
            return numberOfCellsInSec_3
        case 4:
            return numberOfCellsInSec_4
        case 5:
            return numberOfCellsInSec_5
        default:
            return 0
        }
    }
    
}

extension AwarnessMeetingVC:  UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.performBase64EncodingOn(image: pickedImage)
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}
