//
//  pickerCellController.swift
//  No Dumping
//
//  Created by Mahil Arasu on 25/04/17.
//  Copyright © 2017 iqube. All rights reserved.
//

import UIKit

public let notificationCenter = NotificationCenter.default
public let zoneChangeNotification = NSNotification.Name(rawValue: "ZoneChangeNotification")
public let wardsChangeNotification = NSNotification.Name(rawValue: "WardsChangeNotification")


class pickerCellController: UICollectionViewCell {
    
    var chooseIndex = 0
    
    @IBOutlet weak var picker: UIPickerView!
    
    var pickerData: Any!
    var pickerRowcount = 0
    
    //function to know the number of rows in component
    func numberOfrowsForPicker() -> Int {
        if pickerData is [apartmentModel] {
            let apartmentModelObj = pickerData as! [apartmentModel]
            return apartmentModelObj.count
        } else if pickerData is String && pickerData as! String == "Apartment Mixed waste - 'Yes or No'" {
            //Yes or no
            return 2
//            "Ward Zone - 5 zones"
        } else if pickerData is String && pickerData as! String == "Ward management - 5 zones" {
            //North, South, Central, East, West
            return 5
        } else if pickerData is [wardList]{
            let wards = pickerData as! [wardList]
            return wards.count
        } else if pickerData is [pushcartList] {
            let pushcarts = pickerData as! [pushcartList]
            return pushcarts.count
        } else {
            return chooseIndex + 2
        }
    }
    
    //func to know the string to show in the picker
    func pickerStringFor(row: Int) -> String {
        if pickerData is [apartmentModel] {
            let apartmentModelObj = pickerData as! [apartmentModel]
            return apartmentModelObj[row].name
        } else if pickerData is String && pickerData as! String == "Apartment Mixed waste - 'Yes or No'" {
            //Yes or no
            return (row == 0) ? "Yes" : "No"
        } else if pickerData is String && pickerData as! String == "Ward management - 5 zones" {
            
            //North, South, Central, East, West
            switch row {
            case 0:
                return "North"
            case 1:
                return "South"
            case 2:
                return "Central"
            case 3:
                return "East"
            case 4:
                return "West"
            default:
                return "Error"
            }
        } else if pickerData is [wardList] {
            let wards = pickerData as! [wardList]
            return "\(wards[row].no)"
        } else if pickerData is [pushcartList] {
            let pushcarts = pickerData as! [pushcartList]
            return pushcarts[row].name
        } else {
            let returnString = "Hello" + "\(row)"
            return returnString
        }
    }
    
    //func to know the selected component
    func selectedComponentat(row: Int) {
        if pickerData is [apartmentModel] {
            let apartmentModelObj = pickerData as! [apartmentModel]
            apartmentWasteInfo.apartmentName = apartmentModelObj[row].name
            apartmentWasteInfo.apartmentId = apartmentModelObj[row].id
        } else if pickerData is String && pickerData as! String == "Apartment Mixed waste - 'Yes or No'" {
            //Yes or no
            apartmentWasteInfo.mixedWaste = (row == 0) ? "Yes" : "No"
        } else if pickerData is String && pickerData as! String == "Ward management - 5 zones" {
            //North, South, Central, East, West
            switch row {
            case 0:
                wardWasteInfo.zone = "North"
            case 1:
                wardWasteInfo.zone = "South"
            case 2:
                wardWasteInfo.zone = "Central"
            case 3:
                wardWasteInfo.zone = "East"
            case 4:
                wardWasteInfo.zone = "West"
            default:
                wardWasteInfo.zone = "Error"
            }
            notificationCenter.post(name: zoneChangeNotification, object: nil)
        } else if pickerData is [wardList] {
            let wards = pickerData as! [wardList]
            wardWasteInfo.wardNo = "\(wards[row].no)"
            wardWasteInfo.wardId = "\(wards[row].id)"
            notificationCenter.post(name: wardsChangeNotification, object: nil)
        } else if pickerData is [pushcartList] {
            let pushcarts = pickerData as! [pushcartList]
            wardWasteInfo.pushcartName = pushcarts[row].name
            wardWasteInfo.pushcartID = pushcarts[row].id
        }
    }
    
}

extension pickerCellController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return numberOfrowsForPicker()
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.pickerStringFor(row: row)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedComponentat(row: row)
    }
    
}
